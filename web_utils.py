# Copyright (C) 2022 The Qt Company Ltd.
# Contact: https://www.qt.io/licensing/
#
# You may use this file under the terms of the CC0 license.
# See the file LICENSE.CC0 from this package for details.

import requests
from dash import dcc, html


def get_header():
    """
    This is in charge of return the divs that form the header,
    both the left logo/title and the right side menu.
    """
    return html.Div(
        children=[
            html.Div(
                children=[
                    html.A(
                        children=[
                            html.Img(src="/assets/theqtproject.png", className="header-logo")
                        ],
                        href="https://contribute.qt-project.org",
                    ),
                ],
                className="header-title-left",
            ),
            html.Div(
                children=[
                    html.A(children="Code Review", href="https://codereview.qt-project.org/"),
                    html.A(children="Bug Tracker", href="https://bugreports.qt.io"),
                    html.A(children="Wiki", href="https://wiki.qt.io"),
                    html.A(children="Docs", href="https://doc.qt.io"),
                    html.A(children="Mailing List", href="https://lists.qt-project.org"),
                    html.A(children="Forum", href="https://forum.qt.io"),
                    html.A(children="Qt.io", href="https://qt.io", style={"color": "#41CD52"}),
                ],
                className="header-menu-right",
            ),
        ],
        className="header",
    )


def get_column(divs=[], columns_number="six", classes=""):
    """
    This returns the left divs which contain explanatory text
    about different topics, defined by different markdown files
    on this project.
    """

    def get_div(div):
        content, color = div

        _style = {}
        if color is not None:
            _style = {"background-color": color}

        _classes = ["card", "pad"] + classes.split()

        return html.Div(
            children=[
                dcc.Markdown(content),
            ],
            className=" ".join(_classes),
            style=_style,
        )

    content = []
    for div in divs:
        if isinstance(div, tuple):
            content.append(get_div(div))
        else:
            content.append(div)

    return html.Div(children=content, className=f"{columns_number} columns")


def get_file_content(filename, color=None):
    content = ""
    with open(filename, "r") as f:
        content = f.read()
    return content, color


def get_filter(modules, years):
    """
    Get div containing the combobox to filter the modules
    and years, to trigger the charts update.
    """
    return html.Div(
        children=[
            html.Div(
                children=[
                    html.Div(
                        children=[
                            html.Div(children="Module", className="menu-title"),
                            dcc.Dropdown(
                                id="module-filter",
                                options=[{"label": m, "value": m} for m in modules],
                                value="qtbase",
                                clearable=False,
                                className="dropdown",
                            ),
                        ],
                    ),
                    dcc.Checklist(
                        id="tqtc-filter",
                        options=[
                            {"label": "Include commits from The Qt Company", "value": "TQtC"},
                        ],
                        value=["TQtC"],
                        className="tqtc-filter",
                    ),
                ],
                className="five columns",
            ),
            html.Div(
                children=[
                    html.Div(
                        children=[
                            html.Div(children="Starting year", className="menu-title"),
                            dcc.Dropdown(
                                id="year-filter",
                                options=[{"label": m, "value": m} for m in years],
                                value=2018,
                                clearable=False,
                                className="dropdown",
                            ),
                        ],
                    ),
                ],
                className="three columns",
            ),
            html.Div(
                children=[
                    html.Div(
                        children=[
                            html.Div(children="Contributors", className="menu-title"),
                            dcc.Input(
                                id="contributors-filter",
                                value=10,
                                min=5,
                                type="number",
                                className="input-number",
                            ),
                        ],
                    ),
                ],
                className="three columns",
            ),
        ],
        className="row card option-select",
    )

encoded_img = {"mastodon":
               'PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcKICAgcm9sZT0iaW1nIgogICB2aWV3Qm94PSIwIDAgMjQgMjQiCiAgIHZlcnNpb249IjEuMSIKICAgaWQ9InN2ZzEiCiAgIHNvZGlwb2RpOmRvY25hbWU9InF0X21hc3RvZG9uLnN2ZyIKICAgaW5rc2NhcGU6dmVyc2lvbj0iMS40IChlN2MzZmViMTAwLCAyMDI0LTEwLTA5KSIKICAgeG1sbnM6aW5rc2NhcGU9Imh0dHA6Ly93d3cuaW5rc2NhcGUub3JnL25hbWVzcGFjZXMvaW5rc2NhcGUiCiAgIHhtbG5zOnNvZGlwb2RpPSJodHRwOi8vc29kaXBvZGkuc291cmNlZm9yZ2UubmV0L0RURC9zb2RpcG9kaS0wLmR0ZCIKICAgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIgogICB4bWxuczpzdmc9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIgogICB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiCiAgIHhtbG5zOmNjPSJodHRwOi8vY3JlYXRpdmVjb21tb25zLm9yZy9ucyMiCiAgIHhtbG5zOmRjPSJodHRwOi8vcHVybC5vcmcvZGMvZWxlbWVudHMvMS4xLyI+CiAgPGRlZnMKICAgICBpZD0iZGVmczEiIC8+CiAgPHNvZGlwb2RpOm5hbWVkdmlldwogICAgIGlkPSJuYW1lZHZpZXcxIgogICAgIHBhZ2Vjb2xvcj0iIzUwNTA1MCIKICAgICBib3JkZXJjb2xvcj0iI2VlZWVlZSIKICAgICBib3JkZXJvcGFjaXR5PSIxIgogICAgIGlua3NjYXBlOnNob3dwYWdlc2hhZG93PSIwIgogICAgIGlua3NjYXBlOnBhZ2VvcGFjaXR5PSIwIgogICAgIGlua3NjYXBlOnBhZ2VjaGVja2VyYm9hcmQ9IjAiCiAgICAgaW5rc2NhcGU6ZGVza2NvbG9yPSIjZDFkMWQxIgogICAgIGlua3NjYXBlOnpvb209IjQwLjc1IgogICAgIGlua3NjYXBlOmN4PSIyLjgzNDM1NTgiCiAgICAgaW5rc2NhcGU6Y3k9IjkuMzI1MTUzNCIKICAgICBpbmtzY2FwZTp3aW5kb3ctd2lkdGg9IjM4NDAiCiAgICAgaW5rc2NhcGU6d2luZG93LWhlaWdodD0iMjE2MCIKICAgICBpbmtzY2FwZTp3aW5kb3cteD0iMTkyMCIKICAgICBpbmtzY2FwZTp3aW5kb3cteT0iMCIKICAgICBpbmtzY2FwZTp3aW5kb3ctbWF4aW1pemVkPSIxIgogICAgIGlua3NjYXBlOmN1cnJlbnQtbGF5ZXI9InN2ZzEiIC8+CiAgPHRpdGxlCiAgICAgaWQ9InRpdGxlMSI+TWFzdG9kb248L3RpdGxlPgogIDxwYXRoCiAgICAgZD0iTTIzLjI2OCA1LjMxM2MtLjM1LTIuNTc4LTIuNjE3LTQuNjEtNS4zMDQtNS4wMDRDMTcuNTEuMjQyIDE1Ljc5MiAwIDExLjgxMyAwaC0uMDNjLTMuOTggMC00LjgzNS4yNDItNS4yODguMzA5QzMuODgyLjY5MiAxLjQ5NiAyLjUxOC45MTcgNS4xMjcuNjQgNi40MTIuNjEgNy44MzcuNjYxIDkuMTQzYy4wNzQgMS44NzQuMDg4IDMuNzQ1LjI2IDUuNjExLjExOCAxLjI0LjMyNSAyLjQ3LjYyIDMuNjguNTUgMi4yMzcgMi43NzcgNC4wOTggNC45NiA0Ljg1NyAyLjMzNi43OTIgNC44NDkuOTIzIDcuMjU2LjM4LjI2NS0uMDYxLjUyNy0uMTMyLjc4Ni0uMjEzLjU4NS0uMTg0IDEuMjctLjM5IDEuNzc0LS43NTNhLjA1Ny4wNTcgMCAwIDAgLjAyMy0uMDQzdi0xLjgwOWEuMDUyLjA1MiAwIDAgMC0uMDItLjA0MS4wNTMuMDUzIDAgMCAwLS4wNDYtLjAxIDIwLjI4MiAyMC4yODIgMCAwIDEtNC43MDkuNTQ1Yy0yLjczIDAtMy40NjMtMS4yODQtMy42NzQtMS44MThhNS41OTMgNS41OTMgMCAwIDEtLjMxOS0xLjQzMy4wNTMuMDUzIDAgMCAxIC4wNjYtLjA1NGMxLjUxNy4zNjMgMy4wNzIuNTQ2IDQuNjMyLjU0Ni4zNzYgMCAuNzUgMCAxLjEyNS0uMDEgMS41Ny0uMDQ0IDMuMjI0LS4xMjQgNC43NjgtLjQyMi4wMzgtLjAwOC4wNzctLjAxNS4xMS0uMDI0IDIuNDM1LS40NjQgNC43NTMtMS45MiA0Ljk4OS01LjYwNC4wMDgtLjE0NS4wMy0xLjUyLjAzLTEuNjcuMDAyLS41MTIuMTY3LTMuNjMtLjAyNC01LjU0NXptLTMuNzQ4IDkuMTk1aC0yLjU2MVY4LjI5YzAtMS4zMDktLjU1LTEuOTc2LTEuNjctMS45NzYtMS4yMyAwLTEuODQ2Ljc5LTEuODQ2IDIuMzV2My40MDNoLTIuNTQ2VjguNjYzYzAtMS41Ni0uNjE3LTIuMzUtMS44NDgtMi4zNS0xLjExMiAwLTEuNjY4LjY2OC0xLjY3IDEuOTc3djYuMjE4SDQuODIyVjguMTAyYzAtMS4zMS4zMzctMi4zNSAxLjAxMS0zLjEyLjY5Ni0uNzcgMS42MDgtMS4xNjQgMi43NC0xLjE2NCAxLjMxMSAwIDIuMzAyLjUgMi45NjIgMS40OThsLjYzOCAxLjA2LjYzOC0xLjA2Yy42Ni0uOTk5IDEuNjUtMS40OTggMi45Ni0xLjQ5OCAxLjEzIDAgMi4wNDMuMzk1IDIuNzQgMS4xNjQuNjc1Ljc3IDEuMDEyIDEuODEgMS4wMTIgMy4xMnoiCiAgICAgaWQ9InBhdGgxIgogICAgIHN0eWxlPSJmaWxsOiM0MWNkNTI7ZmlsbC1vcGFjaXR5OjEiIC8+CiAgPG1ldGFkYXRhCiAgICAgaWQ9Im1ldGFkYXRhMSI+CiAgICA8cmRmOlJERj4KICAgICAgPGNjOldvcmsKICAgICAgICAgcmRmOmFib3V0PSIiPgogICAgICAgIDxkYzp0aXRsZT5NYXN0b2RvbjwvZGM6dGl0bGU+CiAgICAgIDwvY2M6V29yaz4KICAgIDwvcmRmOlJERj4KICA8L21ldGFkYXRhPgo8L3N2Zz4K',
                "bluesky":
               'PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcKICAgcm9sZT0iaW1nIgogICB2aWV3Qm94PSIwIDAgMjQgMjQiCiAgIHZlcnNpb249IjEuMSIKICAgaWQ9InN2ZzEiCiAgIHNvZGlwb2RpOmRvY25hbWU9InF0X2JsdWVza3kuc3ZnIgogICBpbmtzY2FwZTp2ZXJzaW9uPSIxLjQgKGU3YzNmZWIxMDAsIDIwMjQtMTAtMDkpIgogICB4bWxuczppbmtzY2FwZT0iaHR0cDovL3d3dy5pbmtzY2FwZS5vcmcvbmFtZXNwYWNlcy9pbmtzY2FwZSIKICAgeG1sbnM6c29kaXBvZGk9Imh0dHA6Ly9zb2RpcG9kaS5zb3VyY2Vmb3JnZS5uZXQvRFREL3NvZGlwb2RpLTAuZHRkIgogICB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciCiAgIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciCiAgIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyIKICAgeG1sbnM6Y2M9Imh0dHA6Ly9jcmVhdGl2ZWNvbW1vbnMub3JnL25zIyIKICAgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIj4KICA8ZGVmcwogICAgIGlkPSJkZWZzMSIgLz4KICA8c29kaXBvZGk6bmFtZWR2aWV3CiAgICAgaWQ9Im5hbWVkdmlldzEiCiAgICAgcGFnZWNvbG9yPSIjNTA1MDUwIgogICAgIGJvcmRlcmNvbG9yPSIjZWVlZWVlIgogICAgIGJvcmRlcm9wYWNpdHk9IjEiCiAgICAgaW5rc2NhcGU6c2hvd3BhZ2VzaGFkb3c9IjAiCiAgICAgaW5rc2NhcGU6cGFnZW9wYWNpdHk9IjAiCiAgICAgaW5rc2NhcGU6cGFnZWNoZWNrZXJib2FyZD0iMCIKICAgICBpbmtzY2FwZTpkZXNrY29sb3I9IiNkMWQxZDEiCiAgICAgaW5rc2NhcGU6em9vbT0iODEuNSIKICAgICBpbmtzY2FwZTpjeD0iMTIuMDA2MTM1IgogICAgIGlua3NjYXBlOmN5PSIxMiIKICAgICBpbmtzY2FwZTp3aW5kb3ctd2lkdGg9IjM4NDAiCiAgICAgaW5rc2NhcGU6d2luZG93LWhlaWdodD0iMjE2MCIKICAgICBpbmtzY2FwZTp3aW5kb3cteD0iMTkyMCIKICAgICBpbmtzY2FwZTp3aW5kb3cteT0iMCIKICAgICBpbmtzY2FwZTp3aW5kb3ctbWF4aW1pemVkPSIxIgogICAgIGlua3NjYXBlOmN1cnJlbnQtbGF5ZXI9InN2ZzEiIC8+CiAgPHRpdGxlCiAgICAgaWQ9InRpdGxlMSI+Qmx1ZXNreTwvdGl0bGU+CiAgPHBhdGgKICAgICBkPSJNMTIgMTAuOGMtMS4wODctMi4xMTQtNC4wNDYtNi4wNTMtNi43OTgtNy45OTVDMi41NjYuOTQ0IDEuNTYxIDEuMjY2LjkwMiAxLjU2NS4xMzkgMS45MDggMCAzLjA4IDAgMy43NjhjMCAuNjkuMzc4IDUuNjUuNjI0IDYuNDc5LjgxNSAyLjczNiAzLjcxMyAzLjY2IDYuMzgzIDMuMzY0LjEzNi0uMDIuMjc1LS4wMzkuNDE1LS4wNTYtLjEzOC4wMjItLjI3Ni4wNC0uNDE1LjA1Ni0zLjkxMi41OC03LjM4NyAyLjAwNS0yLjgzIDcuMDc4IDUuMDEzIDUuMTkgNi44Ny0xLjExMyA3LjgyMy00LjMwOC45NTMgMy4xOTUgMi4wNSA5LjI3MSA3LjczMyA0LjMwOCA0LjI2Ny00LjMwOCAxLjE3Mi02LjQ5OC0yLjc0LTcuMDc4YTguNzQxIDguNzQxIDAgMCAxLS40MTUtLjA1NmMuMTQuMDE3LjI3OS4wMzYuNDE1LjA1NiAyLjY3LjI5NyA1LjU2OC0uNjI4IDYuMzgzLTMuMzY0LjI0Ni0uODI4LjYyNC01Ljc5LjYyNC02LjQ3OCAwLS42OS0uMTM5LTEuODYxLS45MDItMi4yMDYtLjY1OS0uMjk4LTEuNjY0LS42Mi00LjMgMS4yNEMxNi4wNDYgNC43NDggMTMuMDg3IDguNjg3IDEyIDEwLjhaIgogICAgIGlkPSJwYXRoMSIKICAgICBzdHlsZT0iZmlsbDojNDFjZDUyO2ZpbGwtb3BhY2l0eToxIiAvPgogIDxtZXRhZGF0YQogICAgIGlkPSJtZXRhZGF0YTEiPgogICAgPHJkZjpSREY+CiAgICAgIDxjYzpXb3JrCiAgICAgICAgIHJkZjphYm91dD0iIj4KICAgICAgICA8ZGM6dGl0bGU+Qmx1ZXNreTwvZGM6dGl0bGU+CiAgICAgIDwvY2M6V29yaz4KICAgIDwvcmRmOlJERj4KICA8L21ldGFkYXRhPgo8L3N2Zz4K',
               "x":
               'PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcKICAgcm9sZT0iaW1nIgogICB2aWV3Qm94PSIwIDAgMjQgMjQiCiAgIHZlcnNpb249IjEuMSIKICAgaWQ9InN2ZzEiCiAgIHNvZGlwb2RpOmRvY25hbWU9InF0X3guc3ZnIgogICBpbmtzY2FwZTp2ZXJzaW9uPSIxLjQgKGU3YzNmZWIxMDAsIDIwMjQtMTAtMDkpIgogICB4bWxuczppbmtzY2FwZT0iaHR0cDovL3d3dy5pbmtzY2FwZS5vcmcvbmFtZXNwYWNlcy9pbmtzY2FwZSIKICAgeG1sbnM6c29kaXBvZGk9Imh0dHA6Ly9zb2RpcG9kaS5zb3VyY2Vmb3JnZS5uZXQvRFREL3NvZGlwb2RpLTAuZHRkIgogICB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciCiAgIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciCiAgIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyIKICAgeG1sbnM6Y2M9Imh0dHA6Ly9jcmVhdGl2ZWNvbW1vbnMub3JnL25zIyIKICAgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIj4KICA8ZGVmcwogICAgIGlkPSJkZWZzMSIgLz4KICA8c29kaXBvZGk6bmFtZWR2aWV3CiAgICAgaWQ9Im5hbWVkdmlldzEiCiAgICAgcGFnZWNvbG9yPSIjNTA1MDUwIgogICAgIGJvcmRlcmNvbG9yPSIjZWVlZWVlIgogICAgIGJvcmRlcm9wYWNpdHk9IjEiCiAgICAgaW5rc2NhcGU6c2hvd3BhZ2VzaGFkb3c9IjAiCiAgICAgaW5rc2NhcGU6cGFnZW9wYWNpdHk9IjAiCiAgICAgaW5rc2NhcGU6cGFnZWNoZWNrZXJib2FyZD0iMCIKICAgICBpbmtzY2FwZTpkZXNrY29sb3I9IiNkMWQxZDEiCiAgICAgaW5rc2NhcGU6em9vbT0iODEuNSIKICAgICBpbmtzY2FwZTpjeD0iMTIuMDA2MTM1IgogICAgIGlua3NjYXBlOmN5PSIxMiIKICAgICBpbmtzY2FwZTp3aW5kb3ctd2lkdGg9IjM4NDAiCiAgICAgaW5rc2NhcGU6d2luZG93LWhlaWdodD0iMjE2MCIKICAgICBpbmtzY2FwZTp3aW5kb3cteD0iMTkyMCIKICAgICBpbmtzY2FwZTp3aW5kb3cteT0iMCIKICAgICBpbmtzY2FwZTp3aW5kb3ctbWF4aW1pemVkPSIxIgogICAgIGlua3NjYXBlOmN1cnJlbnQtbGF5ZXI9InN2ZzEiIC8+CiAgPHRpdGxlCiAgICAgaWQ9InRpdGxlMSI+WDwvdGl0bGU+CiAgPHBhdGgKICAgICBkPSJNMTguOTAxIDEuMTUzaDMuNjhsLTguMDQgOS4xOUwyNCAyMi44NDZoLTcuNDA2bC01LjgtNy41ODQtNi42MzggNy41ODRILjQ3NGw4LjYtOS44M0wwIDEuMTU0aDcuNTk0bDUuMjQzIDYuOTMyWk0xNy42MSAyMC42NDRoMi4wMzlMNi40ODYgMy4yNEg0LjI5OFoiCiAgICAgaWQ9InBhdGgxIgogICAgIHN0eWxlPSJmaWxsOiM0MWNkNTI7ZmlsbC1vcGFjaXR5OjEiIC8+CiAgPG1ldGFkYXRhCiAgICAgaWQ9Im1ldGFkYXRhMSI+CiAgICA8cmRmOlJERj4KICAgICAgPGNjOldvcmsKICAgICAgICAgcmRmOmFib3V0PSIiPgogICAgICAgIDxkYzp0aXRsZT5YPC9kYzp0aXRsZT4KICAgICAgPC9jYzpXb3JrPgogICAgPC9yZGY6UkRGPgogIDwvbWV0YWRhdGE+Cjwvc3ZnPgo='
               }
