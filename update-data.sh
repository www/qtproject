# Copyright (C) 2021 The Qt Company Ltd.
# Contact: https://www.qt.io/licensing/
#
# You may use this file under the terms of the CC0 license.
# See the file LICENSE.CC0 from this package for details.


# For local testing with new data

# Approach to use Git
# If the repository doesn't exist, clone it
if [[ ! -d qt5 ]];then
    git clone https://code.qt.io/qt/qt5.git  || echo "Error cloning 'qt5'"
fi

# Enter the repository
cd qt5

## Clone qt-creator if it doesn't exist
#if [[ ! -d qt-creator ]];then
#    git clone http://code.qt.io/cgit/qt-creator/qt-creator.git || echo "Error cloning 'qt-creator'"
#fi

# Clone pyside-setup if it doesn't exist
if [[ ! -d pyside-setup ]];then
    git clone https://code.qt.io/pyside/pyside-setup || echo "Error cloning 'pyside-setup'"
fi

# General update of all the submodules
git pull -r
git submodule foreach "(git co dev || git co master)"
git submodule foreach git pull --rebase
git submodule update --init -f

## Update 'qt-creator'
#cd qt-creator
#git pull -r
#cd ..

# Update 'pyside-setup'
cd pyside-setup
git pull -r
cd ..

# Generate CSV files
cd ../data
python get_data.py --qt ../qt5
