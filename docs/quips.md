### Qt's Utilitarian Improvement Process (QUIPS)

 * [0001 - QUIP Purpose and Guidelines](http://quips-qt-io.herokuapp.com/quip-0001.html)
 * [0002 - The Qt Governance Model](http://quips-qt-io.herokuapp.com/quip-0002.html)
 * [0003 - "QUIPs for Qt" - QtCon 2016 Session Notes](http://quips-qt-io.herokuapp.com/quip-0003.html)
 * [0004 - Third-Party Components in Qt](http://quips-qt-io.herokuapp.com/quip-0004.html)
 * [0005 - Choosing a Branch](http://quips-qt-io.herokuapp.com/quip-0005.html)
 * [0006 - Acceptable Source-Incompatible Changes](http://quips-qt-io.herokuapp.com/quip-0006.html)
 * [0007 - qt_attribution.json File Format](http://quips-qt-io.herokuapp.com/quip-0007.html)
 * [0009 - Evolving QList](http://quips-qt-io.herokuapp.com/quip-0009.html)
 * [0010 - Reviewing API changes in preparation for release](http://quips-qt-io.herokuapp.com/quip-0010-API-review.html)
 * [0011 - Qt Release Process](http://quips-qt-io.herokuapp.com/quip-0011-Release-Process.html)
 * [0012 - The Qt Community Code of Conduct](http://quips-qt-io.herokuapp.com/quip-0012-Code-of-Conduct.html)
 * [0013 - Qt Examples and Demos](http://quips-qt-io.herokuapp.com/quip-0013-Examples.html)
 * [0014 - The Module Life-Cycle](http://quips-qt-io.herokuapp.com/quip-0014-Module-Criteria.html)
 * [0015 - Qt Project Security Policy](http://quips-qt-io.herokuapp.com/quip-0015-Security-Policy.html)
 * [0016 - Branch policy](http://quips-qt-io.herokuapp.com/quip-0016-branch-policy.html)
 * [0017 - Change log creation](http://quips-qt-io.herokuapp.com/quip-0017-Change-log-creation.html)
