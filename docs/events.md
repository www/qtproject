### Events

[Qt World Summit](https://www.qt.io/qt-world-summit-2025)
* When: May 6th - 7th, 2025
* Where: Showpalast, Munich

[Qt Contributor Summit](https://wiki.qt.io/Qt_Contributor_Summit_2025)
* When: May 7th - 8th, 2025
* Where: TBD, Munich
