### Contribution guidelines

The Qt Project governs the open source development of Qt.
It allows anybody wanting to contribute to join the effort,
through a [meritocratic structure of approvers and maintainers](http://wiki.qt.io/The_Qt_Governance_Model).

All development will be driven by the people contributing to the project.
To learn more, visit the [wiki](https://wiki.qt.io) and
[subscribe to our mailing-lists](http://lists.qt-project.org/).

* [Qt Project Guidelines](https://wiki.qt.io/Qt_Project_Guidelines)
* [Qt source repositories](http://code.qt.io/)
* [Contribution agreement details](http://qt.io/contributionagreement)
