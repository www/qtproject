#### Do Community Work

There are plenty of important tasks to keep things going smoothly within the larger Qt community.
No matter if you prefer online or IRL interactions, there is something for you.

* [Represent Qt at events and meetups](http://wiki.qt.io/Meet_Qt)
* [Organize events and meetups](http://www.meetup.com/QtEverywhere/)
