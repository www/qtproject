#### Write Qt Code

Naturally, the Qt Project is mostly about code.
There are plenty of ways to contribute code, to learn and grow,
and to [build one's reputation](http://wiki.qt.io/Qt_Governance_Model)
To get started contributing code, get in touch with
[the relevant module maintainer](http://wiki.qt.io/Maintainers) before you start on a patch.

* [Fix bugs](https://bugreports.qt.io)
* [Write tests](http://wiki.qt.io/Writing_Unit_Tests)
* [Review Qt code](http://wiki.qt.io/Code_Reviews)
* [Write Qt code](http://wiki.qt.io)
* [Participate in the release process](http://wiki.qt.io/Release_Management)
