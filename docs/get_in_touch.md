### Get in touch

Looking for contact other Qt developers?

Qt also has [numerous channels](https://wiki.qt.io/Online_Communities) on the
[Libera.chat network](https://libera.chat), some of which are also bridged to
[KDE's Matrix instance](https://webchat.kde.org/).

Additionally, you can find help in the [Qt Forum](https://forum.qt.io).
