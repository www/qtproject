### General Information

This page is focused on the contribution and community aspects of the Qt
Project, in case you are looking for information related to The Qt Company
visit [qt.io](https://qt.io).

The **Qt Project** is an open collaboration effort to coordinate the
development of the Qt software framework and tools.

The **Qt Company** oversees the development of the Qt framework alongside the
**Qt Project**, and holds the rights to the Commercial Licenses of the
framework.
