# Qt Project

Dash application behind the `qt-project.org` website.

Currently there are two main goals that this page wants to achieve:

  1. Help people contribute to Qt,
  2. Show statistics about the project.

## Setup

Create a new Python virtual environment and install the requirements:

```
python -m venv env        # might be 'python3' depending on your system

source env/bin/activate   # macOS/Linux
env\Scripts\activate.bat  # windows

pip install -r requirements.txt
```

## Getting and processing the data

The `data/` directory contains a script to get the information
from a local `qt5.git` repository, and you can use it like this:

```
python get_data.py --qt /path/to/my/qt5.git
```

this will generate many `<module>.csv` files with the following columns

```
date;sha;name;original_email;email;domain;files_changed;insertions;deletions
```

Currently, there are also two external repositories being cloned, and used
inside the `qt5.git`, the Qt Creator and PySide repositories.

> The difference between `original_email` and `email` is because many old
> Nokia, Digia, theqtcompany emails were transformed into `@qt.io`.

## Starting the application

The website is based on a framework called [Dash](https://plotly.com/dash/),
which enables the creation of Dashboard.

We also rely on text-based information. There are `markdown` files
which contains the information displayed on the left-columns, like `guidelines.md`
and `contribute.md`.

To start the application, run:
```
python app.py
```

which you can verify on [http://localhost:8050](http://localhost:8050).

Additionally, there is a `Procfile` to enable the site to be deployed
on servers compatible with [gunicorn](https://gunicorn.org/).
